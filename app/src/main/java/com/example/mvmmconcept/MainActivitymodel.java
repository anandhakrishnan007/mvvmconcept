package com.example.mvmmconcept;

import android.app.Activity;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

public class MainActivitymodel extends BaseObservable {
    Activity mActivity;
    @Bindable
    String textviewtext;
    @Bindable
    boolean wantshow = true;
    int i = 0;
static  AppCompatEditText currentedittext;
    public MainActivitymodel(Activity mActivity) {
        this.mActivity = mActivity;
    }



//Access editecty directly from xml file like bellow
   // AppCompatEditText currentedittext=(AppCompatEditText) findViewById(R.id.edttext);

    @BindingAdapter(value = {"android:Myedittext"}, requireAll = false)
    public static void setIndicator(AppCompatEditText edittext, String adapter) {
        currentedittext=edittext;

    }

    public void setmActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    public String getTextviewtext() {
        return textviewtext;
    }

    public void setTextviewtext(String textviewtext) {
        this.textviewtext = textviewtext;
        notifyPropertyChanged(com.example.mvmmconcept.BR.textviewtext);
    }

    public boolean isWantshow() {
        return wantshow;
    }

    public void setWantshow(boolean wantshow) {
        this.wantshow = wantshow;
        notifyPropertyChanged(com.example.mvmmconcept.BR.wantshow);
    }

    public void increement() {
        i = i + 1;
        setTextviewtext("I Values = " + i);

    }

    public void decreement() {
        if (i > 0) {
            i = i - 1;
            setTextviewtext("I Values = " + i);
        }
    }

    public void edittextfield(CharSequence s, int type) {
        i = Integer.parseInt(s.toString());
    }
}

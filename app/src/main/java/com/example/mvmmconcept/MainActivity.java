package com.example.mvmmconcept;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.mvmmconcept.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
MainActivitymodel viewmodel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_main);
        viewmodel=new MainActivitymodel(this);
        ActivityMainBinding activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setViewmodel(viewmodel);
        activityMainBinding.executePendingBindings();


    }
}
